import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import ListEmployeesComponent from "./components/employee/ListEmployeesComponent";
import AddEmployeeComponent from "./components/employee/AddEmployeeComponent";
import EditEmployeeComponent from "./components/employee/EditEmployeeComponent";
import AddDepartmentComponent from "./components/department/AddDepartmentComponent";
import ListDepartmentsComponent from "./components/department/ListDepartmentsComponent";
import EditDepartmentComponent from "./components/department/EditDepartmentComponent";

import React from "react";
function App() {
  return (
      <div className="container">
          <Router>
              <div className="col-md-6">
                  <h1 className="text-center" className="App-header" style={style}> Employee CRUD operation </h1>
                  <Switch>
                      <Route path="/" exact component={ListEmployeesComponent} />
                      <Route path="/users" component={ListEmployeesComponent} />
                      <Route path="/add-user" component={AddEmployeeComponent} />
                      <Route path="/edit-user" component={EditEmployeeComponent} />
                      <Route path="/add-dep" component={AddDepartmentComponent} />
                      <Route path="/list-dep" component={ListDepartmentsComponent} />
                      <Route path="/edit-dep" component={EditDepartmentComponent} />

                  </Switch>
              </div>
          </Router>
      </div>
  );
}

const style = {
    color: 'white',
    margin: '100px',
}

export default App;
