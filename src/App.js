import React from 'react';
import './App.css';
import AppRouter from "./RouterComponent";
import NavBar from "./Navbar";
import Container from '@material-ui/core/Container';

function App() {
  return (
      <div className="container">
          {/* <Router>
              <div className="col-md-6">
                  <h1 className="text-center" className="App-header" style={style}> Employee CRUD operation </h1>
                  <Switch>
                      <Route path="/" exact component={ListUserComponent} />
                      <Route path="/users" component={ListUserComponent} />
                      <Route path="/add-user" component={AddEmployeeComponent} />
                      <Route path="/edit-user" component={EditEmployeeComponent} />
                  </Switch>
              </div>
          </Router> */}
            <Container>
                <AppRouter/>
          </Container>
      </div>
  );
}

const style = {
    color: 'white',
    margin: '100px',
}

export default App;
