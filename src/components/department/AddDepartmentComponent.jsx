import React, { Component } from 'react'
import ApiService from "../../service/ApiService";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import NativeSelect from '@material-ui/core/NativeSelect';
import InputBase from '@material-ui/core/InputBase';

class AddDepartmentComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            departmentID: '',
            departmentName: '',
            managerID: '',
            managers: []
        }
        this.saveDepartment = this.saveDepartment.bind(this);
        this.reloadDeparmentList = this.reloadDeparmentList.bind(this);
        this.handleManagerSelect = this.handleManagerSelect.bind(this);
        this.getManagersItems = this.getManagersItems.bind(this);
    }

    componentDidMount() {
        this.getManagersItems();
    }

    reloadDeparmentList() {
        ApiService.fetchDeparments()
            .then((res) => {
                if (res.data.length != 0) {
                    for (var i = 0; i < res.data.length; i++) {
                        this.state.departments.push(res.data[i]);
                    }
                    this.setState(this.state.departments);
                }
            });
    }
   

    saveDepartment = (e) => {
        e.preventDefault();
        let department = {// departmentID: this.state.departmentID,
            departmentName: this.state.departmentName, 
            manager: {employeeID: this.state.managerID}
        };
        ApiService.addDepartment(department)
            .then(res => {
                this.setState({ message: 'Department added successfully.' });
            });
    }
  

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

        //get Departments list
        handleManagerSelect(event) {
            console.log(event)
            this.setState({ employeeID: event.target.value });
            this.setState({ managerID: event.target.value });
        }
        rendeMenueManagerItem() {
            return this.state.managers.map((emp, i) => {
                return (
                    <MenuItem
                        key={i}
                        value={emp.employeeID}>
                        {emp.firstName + ' ' + emp.lastName}
                    </MenuItem>
                );
            });
        }
    
        getManagersItems() {
            ApiService.fetchEmployess()
                .then((res) => {
                    if (res.data.length != 0) {
                        for (var i = 0; i < res.data.length; i++) {
                            this.state.managers.push(res.data[i]);
                        }
                        this.setState(this.state.managers);
                    }
                });
        }
    
       backToListDepartments() {
            this.props.history.push('/list-dep');
        }

    render() {
        return (

            <div>
                <Typography variant="h4" style={style}>Add Department</Typography>

                <form>

                    {/* <TextField type="text" placeholder="Department ID" fullWidth margin="normal" name="departmentID" value={this.state.departmentID} onChange={this.onChange} /> */}

                    <TextField type="text" placeholder="Department Name" fullWidth margin="normal" name="departmentName" value={this.state.departmentName} onChange={this.onChange} />

                    {/* <TextField ype="text" placeholder="manager ID" fullWidth margin="normal" name="managerID" value={this.state.managerID} onChange={this.onChange} /> */}

                      <Select fullWidth
                        value={this.state.employeeID}
                        onChange={this.handleManagerSelect}
                    >
                        {this.rendeMenueManagerItem()}
                    </Select>

                    <Button variant="contained" type="submit" color="primary" onClick={this.saveDepartment}>Save</Button>

                    <Button variant="contained" color="primary" onClick={() => this.backToListDepartments()}>
                    Back
                    </Button>
                </form>

            </div>
        );
    }
}
const style = {
    display: 'flex',
    justifyContent: 'center'
}
export default AddDepartmentComponent;