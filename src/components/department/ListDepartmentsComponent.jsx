import React, { Component } from 'react'
import ApiService from "../../service/ApiService";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import CreateIcon from '@material-ui/icons/Create';
import DeleteIcon from '@material-ui/icons/Delete';
import Typography from '@material-ui/core/Typography';
import Pagination from "../../common/pagination";
import { paginate } from "../../utils/utils";

class ListDepartmentsComponent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            deparments: [],
            message: null,
            pageSize: 3,
            currentPage: 1
        }
        this.deleteDepartment = this.deleteDepartment.bind(this);
        this.editDepartment = this.editDepartment.bind(this);
        this.addDepartment = this.addDepartment.bind(this);
        this.reloadDepartmentsList = this.reloadDepartmentsList.bind(this);
        this.setManagerName = this.setManagerName.bind(this);
    }

    componentDidMount() {
        this.reloadDepartmentsList();
    }

    reloadDepartmentsList() {
        ApiService.fetchDepartments()
            .then((res) => {
                if(res.data.length != 0){
                    for(var i = 0 ; i < res.data.length ; i++ ){
                        this.state.deparments.push(res.data[i]);                   
                    }
                    this.setState( this.state.deparments);
                }
                console.log(res.data);
            });
    }

    deleteDepartment(depId) {
        ApiService.deleteDepartment(depId)
           .then(res => {
               this.setState({message : 'Department deleted successfully.'});
               this.setState({deparments: this.state.deparments.filter(dep => dep.departmentID !== depId)});
           })

    }

    editDepartment(depId) {
        window.localStorage.setItem("depId", depId);
        this.props.history.push('/edit-dep');
    }

    addDepartment() {
        window.localStorage.removeItem("depId");
        this.props.history.push('/add-dep');
    }

    handlePageChange = page => {
        this.setState({
          currentPage: page
        });
        console.log(page);
      };

      setManagerName(employee) {
        
        if(employee.manager == null || employee.manager.employeeID ==null)
          return '';
        else{
           return employee.manager.firstName +' '+ employee.manager.lastName;
        }       
    }

    backToHome() {
        this.props.history.push('/users');
    }

    render() {
        // const { length:count } = this.state.deparments;
        // if (count === 0)
        // return <h2 className="font-weight-bold">There are no Departments in the database.</h2>;
        const departmentList = paginate(
            this.state.deparments,
            this.state.currentPage,
            this.state.pageSize
          );
        return ( 
            <div>
                <Typography variant="h4" style={style}>Deparment Details</Typography>
               

                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell align="right">Deparment ID</TableCell>
                            <TableCell align="right">Deparment Name</TableCell>
                            <TableCell align="left">Manager Name</TableCell>
                           
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {departmentList.map(row => (
                            <TableRow key={row.departmentID}>
                                <TableCell component="th" scope="row">{row.departmentID} </TableCell>
                                <TableCell align="right" >{row.departmentId}</TableCell>
                                <TableCell >{row.departmentName}</TableCell>
                                <TableCell  align="left">{this.setManagerName(row)}</TableCell>
                                <TableCell align="right" onClick={() => this.editDepartment(row.departmentID)}><CreateIcon /></TableCell>
                                <TableCell align="right" onClick={() => this.deleteDepartment(row.departmentID)}><DeleteIcon /></TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
                <Pagination
          itemCount={this.state.deparments.length}
          pageSize={this.state.pageSize}
          onPageChange={this.handlePageChange}
          currentPage={this.state.currentPage}
        />
    <Button variant="contained" color="primary" onClick={() => this.addDepartment()}>
                    Add Deparment
                </Button>

                <Button variant="contained" color="primary" onClick={() => this.backToHome()}>
                    Back
                </Button>
            </div>
        );
    }

}
const style ={
    display: 'flex',
    justifyContent: 'center'
}

export default ListDepartmentsComponent;