import React, { Component } from 'react'
import ApiService from "../../service/ApiService";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import NativeSelect from '@material-ui/core/NativeSelect';
import InputBase from '@material-ui/core/InputBase';

class EditDepartmentComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {

            managers: [],
            departmentId: '',
            departmentName: '',
            managerName:'',
            managerId: '',
        }
        this.editDepartment = this.editDepartment.bind(this);
        this.loadDepartment = this.loadDepartment.bind(this);
        this.setManagerId = this.setManagerId.bind(this);

    }
    componentDidMount() {
        this.loadDepartment();
    }

    editDepartment = (e) => {
        e.preventDefault();
        let department = {
            departmentID: this.state.departmentID,
            departmentName: this.state.departmentName,
            manager: { employeeID: this.state.managerId }
        };

        ApiService.editDepartment(department)
            .then(res => {
                this.setState({ message: 'Department updated successfully.' });
            });
    }


    loadDepartment() {
        ApiService.getDepartmentById(window.localStorage.getItem("depId"))
            .then((res) => {
                let department = res.data;
                this.setState({
                    departmentID: department.departmentID,
                    departmentName: department.departmentName,
                    managerName: this.setManagerId(department),
                    managerId: department.manager.employeeID
                })
            });
    }

    setManagerId(department) {

        if (department.manager == null || department.manager.employeeID == null)
            return '';
        else {
            return department.manager.firstName + ' ' + department.manager.lastName;
        }
    }

    //get Departments list
    handleManagerSelect(event) {
        // this.setState({ employeeID: event.target.value });
        this.setState({ managerId: event.target.value });
    }
    rendeMenueManagerItem() {
        return this.state.managers.map((emp, i) => {
            return (
                <MenuItem
                    key={i}
                    value={emp.employeeID}>
                    {emp.firstName + ' ' + emp.lastName}
                </MenuItem>
            );
        });
    }

    getManagersItems() {
        ApiService.fetchEmployess()
            .then((res) => {
                if (res.data.length != 0) {
                    for (var i = 0; i < res.data.length; i++) {
                        this.state.managers.push(res.data[i]);
                    }
                    this.setState(this.state.managers);
                }
            });
    }


    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    backToListDepartments() {
        this.props.history.push('/list-dep');
    }

    render() {
        const { readOnly } = this.state;
        return (

            <div>
                <Typography variant="h4" style={style}>Edit Department</Typography>

                <form>

                    {/* <TextField type="text" placeholder="  department ID" fullWidth margin="normal" name="departmentID" value={this.state.departmentID} inputProps={{readOnly: Boolean(readOnly),disabled: Boolean(readOnly)}} onChange={this.onChange} /> */}

                    <TextField type="text" placeholder="Department Name" fullWidth margin="normal" name="departmentName" value={this.state.departmentName} onChange={this.onChange} />

                    <Select fullWidth
                        value={this.state.managerId}
                        onChange={this.handleManagerSelect}
                    >
                        {this.rendeMenueManagerItem()}
                    </Select>

                    <Button variant="contained" color="primary" onClick={this.editDepartment}>Update</Button>
                    <Button variant="contained" color="primary" onClick={() => this.backToListDepartments()}>
                        Back
                    </Button>
                </form>



            </div>);
    }

}

const style = {
    display: 'flex',
    justifyContent: 'center'
}
export default EditDepartmentComponent;