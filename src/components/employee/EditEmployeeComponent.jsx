import React, { Component } from 'react'
import ApiService from "../../service/ApiService";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import NativeSelect from '@material-ui/core/NativeSelect';
import InputBase from '@material-ui/core/InputBase';

class EditEmployeeComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            employees: [],
            employeeID: '',
            firstName: '',
            lastName: '',
            salary: '',
            phoneNumber: '',
            email: '',
            hireDate: '',
            departmentId: '',
            managerId: '',
            readOnly: true,
            departmentItems: [],
            managers: [],

        }
        this.editEmployee = this.editEmployee.bind(this);
        this.validateform = this.validateform.bind(this);
        this.loadEmployee = this.loadEmployee.bind(this);
        this.getDepartmentItems = this.getDepartmentItems.bind(this);
        this.handleDepartmentSelect = this.handleDepartmentSelect.bind(this);
        this.handleManagerSelect = this.handleManagerSelect.bind(this);
        this.getManagersItems = this.getManagersItems.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.setManagerId = this.setManagerId.bind(this);
    }
    componentDidMount() {
        this.loadEmployee();
        this.getDepartmentItems();
        this.getManagersItems();
    }

    editEmployee = (e) => {
        e.preventDefault();
        let employee = {
            employeeID: this.state.employeeID, firstName: this.state.firstName, lastName: this.state.lastName,
            salary: this.state.salary, email: this.state.email, phoneNumber: this.state.phoneNumber,
            hireDate: this.state.hireDate, departmentID: {departmentId: this.state.departmentId},
            manager: { employeeID: this.state.managerId }
        };//  
        this.validateform(employee);
        ApiService.editEmployee(employee)
            .then(res => {
                this.setState({ message: 'Employee updated successfully.' });
            });
    }


    validateform(employee) {

        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(employee.email)
            && employee.salary > 0 && employee.firstName.length > 2 && employee.lastName.length > 2) {
            alert(`Employee updated successfully`);
            return (true)
        } else {
            alert(`You have update an invalid email address or salary less than zero or last or first name less than 2 !`);
            return (false)
        }
    }

    setManagerId(employee) {
        
        if(employee.manager == null || employee.manager.employeeID ==null)
          return '';
        else{
           return employee.manager.employeeID;
        }       
    }

    setDepartmentId(employee) {
        
        if(employee.department == null || employee.department.departmentID ==null)
          return '';
        else{
           return employee.department.departmentID;
        }       
    }

    

    loadEmployee() {
        ApiService.getEmployeeById(window.localStorage.getItem("empId"))
            .then((res) => {
                let employee = res.data;
                this.setState({
                    employeeID: employee.employeeID,
                    username: employee.username,
                    firstName: employee.firstName,
                    lastName: employee.lastName,
                    salary: employee.salary,
                    phoneNumber: employee.phoneNumber,
                    email: employee.email,
                    hireDate: employee.hireDate,
                    departmentId: this.setDepartmentId(employee),
                    managerId: this.setManagerId(employee)
                })
            });
    }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    //start render selects
    handleChange = (name, event) => {
        const target = event.target;
        this.setState({
            [name]: event.target.value
        });
    };

    getDepartmentItems() {
        ApiService.fetchDepartments()
            .then((res) => {
                if (res.data.length != 0) {
                    for (var i = 0; i < res.data.length; i++) {
                        this.state.departmentItems.push(res.data[i]);
                    }
                    this.setState(this.state.departmentItems);
                }
            });
    }

    handleDepartmentSelect(event) {
        this.setState({ departmentID: event.target.value });
    }
    rendeMenueItem() {
        return this.state.departmentItems.map((dep, i) => {
            return (
                <MenuItem
                    key={i}
                    value={dep.departmentID}>
                    {dep.departmentName}
                </MenuItem>
            );
        });
    }





    handleManagerSelect(event) {
        this.setState({ managerId: event.target.value });
    }
    rendeMenueManagerItem() {
        return this.state.managers.map((emp, i) => {
            return (
                <MenuItem
                    key={i}
                    value={emp.employeeID}>
                    {emp.firstName + ' ' + emp.lastName}
                </MenuItem>
            );
        });
    }

    getManagersItems() {
        ApiService.fetchEmployess()
            .then((res) => {
                if (res.data.length != 0) {
                    for (var i = 0; i < res.data.length; i++) {
                        this.state.managers.push(res.data[i]);
                    }
                    this.setState(this.state.managers);
                }
            });
    }

    //end render selects

    backToHome() {
        this.props.history.push('/users');
    }

    render() {
        const { readOnly } = this.state;
        return (

            <div>
                <Typography variant="h4" style={style}>Edit Employee</Typography>

                <form>

                    <TextField type="text" placeholder="employee ID" fullWidth margin="normal" name="employeeID" value={this.state.employeeID} inputProps={{ readOnly: Boolean(readOnly), disabled: Boolean(readOnly) }} onChange={this.onChange} />

                    <TextField type="text" placeholder="First Name" fullWidth margin="normal" name="firstName" value={this.state.firstName} onChange={this.onChange} />

                    <TextField ype="text" placeholder="Last name" fullWidth margin="normal" name="lastName" value={this.state.lastName} onChange={this.onChange} />

                    <TextField type="number" placeholder="salary" fullWidth margin="normal" name="salary" value={this.state.salary} onChange={this.onChange} />

                    <TextField type="number" placeholder="phoneNumber" fullWidth margin="normal" name="phoneNumber" value={this.state.phoneNumber} onChange={this.onChange} />

                    <TextField type="email" placeholder="email" fullWidth margin="normal" name="email" value={this.state.email} onChange={this.onChange} />

                   <TextField
                        id="datetime-local" fullWidth
                        type="date"
                        onChange={(event) => this.handleChange("hireDate", event)}
                        value={this.state.hireDate}
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />

                    <Select fullWidth
                        value={this.state.departmentID}
                        onChange={this.handleDepartmentSelect}
                    >
                        {this.rendeMenueItem()}
                    </Select>

                    <Select fullWidth
                        value={this.state.managerId}
                        onChange={this.handleManagerSelect}
                    >
                        {this.rendeMenueManagerItem()}
                    </Select>


                    <Button variant="contained" color="primary" onClick={this.editEmployee}>Update</Button>
                    <Button variant="contained" color="primary" onClick={() => this.backToHome()}>
                    Back
                    </Button>
                </form>

            </div>);
    }

}

const style = {
    display: 'flex',
    justifyContent: 'center'
}
export default EditEmployeeComponent;