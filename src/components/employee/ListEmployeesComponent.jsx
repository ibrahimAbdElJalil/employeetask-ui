import React, { Component } from 'react'
import ApiService from "../../service/ApiService";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import CreateIcon from '@material-ui/icons/Create';
import DeleteIcon from '@material-ui/icons/Delete';
import Typography from '@material-ui/core/Typography';
import Pagination from "../../common/pagination";
import { paginate } from "../../utils/utils";

class ListEmployeesComponent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            employees: [],
            message: null,
            pageSize: 3,
            currentPage: 1
        }
        this.deleteEmployee = this.deleteEmployee.bind(this);
        this.editEmployee = this.editEmployee.bind(this);
        this.addEmployee = this.addEmployee.bind(this);
        this.listDepartments = this.listDepartments.bind(this);
        this.reloadEmployeesList = this.reloadEmployeesList.bind(this);
    }

    componentDidMount() {
        this.reloadEmployeesList();
    }

    reloadEmployeesList() {
        ApiService.fetchEmployess()
            .then((res) => {
                if(res.data.length != 0){
                    for(var i = 0 ; i < res.data.length ; i++ ){
                        this.state.employees.push(res.data[i]);                   
                    }
                    this.setState( this.state.employees);
                }
            });
    }

    deleteEmployee(empId) {
        ApiService.deleteEmployee(empId)
           .then(res => {
            this.props.history.push('/users');
               this.setState({message : 'Employee deleted successfully.'});
               this.setState({employees: this.state.employees.filter(employee => employee.employeeID !== empId)});
           })

    }

    editEmployee(id) {
        window.localStorage.setItem("empId", id);
        this.props.history.push('/edit-user');
    }

    addEmployee() {
        window.localStorage.removeItem("userId");
        this.props.history.push('/add-user');
    }

    listDepartments() {
        // window.localStorage.removeItem("userId");
        this.props.history.push('/list-dep');
    }

    handlePageChange = page => {
        this.setState({
          currentPage: page
        });
      };

      setManagerName(employee) {
        
        if(employee.manager == null || employee.manager.employeeID ==null)
          return '';
        else{
           return employee.manager.firstName+' ' +employee.manager.lastName;
        }       
    }
    setDepartmentName(employee) {
        if(employee.department == null || employee.department.departmentID ==null)
        return '';
      else{
         return employee.department.departmentName;
      }
    }

    render() {
        // const { length:count } = this.state.employees;
        // if (count === 0)
        // return <h2 className="font-weight-bold">There are no Employees in the database.</h2>;
        const employeeList = paginate(
            this.state.employees,
            this.state.currentPage,
            this.state.pageSize
          );
        return ( 
            <div>
                <Typography variant="h4" style={style}>Employee Details</Typography>
                <Button variant="contained" color="primary" onClick={() => this.addEmployee()}>
                    Add Employee
                </Button>

                 <Button variant="contained" color="secondary" onClick={() => this.listDepartments()}>
                     Departments
                </Button>

                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>EmployeeID</TableCell>
                            <TableCell>FirstName</TableCell>
                            <TableCell align="right">LastName</TableCell>
                            <TableCell align="right">Email</TableCell>
                            <TableCell align="right">Phone Number</TableCell>
                            <TableCell align="right">Salary</TableCell>
                            <TableCell align="right">Hire Date</TableCell>
                            <TableCell align="right">DepartmentName</TableCell>
                            <TableCell align="right">ManagerName</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {employeeList.map(row => (
                            <TableRow key={row.employeeID}>
                                <TableCell component="th" scope="row">
                                    {row.employeeID}
                                </TableCell>
                                <TableCell align="right">{row.firstName}</TableCell>
                                <TableCell align="right">{row.lastName}</TableCell>
                                <TableCell align="right">{row.email}</TableCell>
                                <TableCell align="right">{row.phoneNumber}</TableCell>
                                <TableCell align="right">{row.salary}</TableCell>
                                <TableCell align="right">{row.hireDate}</TableCell>
                                <TableCell align="right">{this.setDepartmentName(row)}</TableCell>
                                <TableCell align="right">{this.setManagerName(row)}</TableCell>
                                <TableCell align="right" onClick={() => this.editEmployee(row.employeeID)}><CreateIcon /></TableCell>
                                <TableCell align="right" onClick={() => this.deleteEmployee(row.employeeID)}><DeleteIcon /></TableCell>

                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
                <Pagination
          itemCount={this.state.employees.length}
          pageSize={this.state.pageSize}
          onPageChange={this.handlePageChange}
          currentPage={this.state.currentPage}
        />

            </div>
        );
    }

}
const style ={
    display: 'flex',
    justifyContent: 'center'
}

export default ListEmployeesComponent;