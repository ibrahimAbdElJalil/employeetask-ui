import axios from 'axios';

const EMPLOYEE_API_BASE_URL = 'http://localhost:8081/employee';
const DEPARTMENT_API_BASE_URL = 'http://localhost:8081/department';

class ApiService {

    
    

    fetchEmployess() {
        return axios.get(EMPLOYEE_API_BASE_URL+`/readAll`);
    }

    getEmployeeById(empId) {
        return axios.get(EMPLOYEE_API_BASE_URL + '/readOne/' + empId);
    }

    deleteEmployee(empId) {
        return axios.delete(EMPLOYEE_API_BASE_URL + '/delete/' + empId);
    }

    addEmployee(employee) {
        return axios.post(EMPLOYEE_API_BASE_URL+`/createOrUpdate`,employee);            

    }

    editEmployee(employee) {
        return axios.post(EMPLOYEE_API_BASE_URL +`/createOrUpdate`, employee);
    }

    addDepartment(department) {
        return axios.post(DEPARTMENT_API_BASE_URL+`/createOrUpdate`,department);            

    }

    
    fetchDepartments() {
        return axios.get(DEPARTMENT_API_BASE_URL+`/readAll`);
    }

    editDepartment(department) {
        return axios.post(DEPARTMENT_API_BASE_URL +`/createOrUpdate`, department);
    }


    getDepartmentById(depId) {
        return axios.get(DEPARTMENT_API_BASE_URL + '/readOne/' + depId);
    }

    deleteDepartment(depId) {
        return axios.delete(DEPARTMENT_API_BASE_URL + '/delete/' + depId);
    }

}

export default new ApiService();